\documentclass{article}
\usepackage[margin=1.5in]{geometry}
\usepackage{isotope}
\addbibresource{references.bib}

\title{Dependent Types with Borrowing}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

\begin{abstract}
    Dependent type theory and linear typing are two of the most influential ideas in modern computer science research. Recent work, such as \cites{lindep}{lindep-syn}{quantumlinear}, has allowed us to integrate linear types with dependent types, to obtain linear dependent types. Linear types have recently found widespread mainstream applications as part of the Rust programming language, which extends them with the concepts of "borrowing" for a "lifetime," allowing us to check memory-safety, thread-safety, and complex user-defined invariants (such as, e.g., holding a mutex lock while a resource is accessed) at zero runtime cost. The goal of the "Dependent Types with Borrowing" project is to design and implement a simple language with a system of linear dependent types supporting a Rust-like concept of lifetimes and borrowing.
\end{abstract}

\section{Background and Motivation}

By providing fine-grained control over the flow of data within a proof, linear types allow us to model changing state, time, and resources through the concept of "ownership" and hence have found many applications in modeling systems relying on destructive updates, from concurrent imperative processes \cite{lindep} to quantum ones \cites{quantumlinear}{quantalg}. LDTs allow us to perform such modeling internally within a dependent type theory \cite{lindep}, presenting the possibility of extending the Curry-Howard correspondence to imperative programs in a more natural way while simultaneously obtaining precise functional specifications of the behavior of those programs \cite{lindep}.

Outside the world of dependent type theory, one of the most promising uses of linear typing in programming language design is the Rust language's system of \textit{borrowing} \cites{rust}{rustbelt}{stackedborrows}. By extending an affine type system with the ability to borrow'' resources without consuming them, the Rust language allows the programmer to perform complex, machine-checked resource management without using a garbage collector, allowing statically-checked memory-safety and thread-safety without runtime performance overhead. Unfortunately, for some low-level tasks and primitives, Rust programmers must drop down to ``\tty{unsafe}" code, which allows the manipulation of raw pointers but, consequently, comes with all the risks of C \cites{rust}{rustbelt}{stackedborrows}, as ownership and borrowing rules are left unchecked. The RustBelt project (\cite{rustbelt}) has focused on using the concurrent separation logic Iris \cite{iris} to prove the soundness of the language's guarantees; namely, that if all \tty{unsafe} code in a program is Undefined Behaviour (UB) free, then the entire program is so as well. This guarantee allows developers to write innovative APIs, such as the mutex guard example in the abstract, having safe usage checked by the compiler.

Existing research, e.g., on liquid types \cites{liquid}{liquid-haskell}, has explored using (often limited) dependent typing both to verify program specifications. In particular, ``unsafe" fragments, such as unchecked bounds accesses, can be asserted UB-free in an otherwise safe language such as Haskell \cite{liquid-haskell} and ML \cite{liquid}, as well as entire programs in an unsafe language such as C \cite{deputy}. More interestingly, such systems also allow us to verify program correctness and adherence to specification, often with the ability to use an SAT-solver to do so semi-automatically. In this vein, the strict ownership and borrowing discipline imposed by the Rust language encodes the information necessary to generate proofs of safety properties automatically; the Prusti \cite{prusti} project takes this observation one step further and attempts to extract ownership and framing-related information from Rust programs, allowing user-friendly proofs in separation logic.

By combining dependent types with borrowing, we hope to allow a more easily manipulated representation of low-level programs with explicit resource management as dependently typed programs in a manner that makes ownership and framing-related information transparently available to both the compiler and type system. In this way, we hope to enrich dependently-typed models of imperative programs with clear ownership and framing data, allowing easier verification and analysis. In particular, taking inspiration from the RVSDG IR \cite{rvsdg}, we aim to work towards a highly normalizing dataflow-like representation similar to modern optimizing compiler IRs. In the future, we hope this will allow for a natural transformation from programs in a Rust-like language to a dependently-typed model, and eventually, the ability to extend Rust-like languages with dependent types directly by providing a practical ``low-level imperative dependent type theory."

\section{Research Questions and Methodology}

The main goal of this project is to explore the design space of combining dependent types with borrowing. To do so, we introduce \tty{isotope}: a bare-bones dependently-typed intermediate representation with borrowing. 
The design of \tty{isotope} is based on the RVSDG IR described in \cite{rvsdg}, which we extend with dependent types using a method analogous to that in \cite{lindep}. Lifetimes are modeled by drawing ``temporal edges'' between RVSDG nodes determined by dependencies on borrowed resources; borrow checking can then be reduced to the problem of cycle-checking the resulting graph (to detect inconsistent execution order requirements). This is somewhat different to Rust's approach, in that lifetimes are essential to the program's execution semantics and hence must be considered by a compiler backend when emitting code, versus in Rust, where they may be ignored after the type-checking stage.

The main research goal of my thesis would be the design and implementation of \tty{isotope}, which I would base on a prototype, \tty{rain}, that I wrote in the previous summer with a partner Qingyuan Qie \cite{rain-lang}. While I have begun implementing \tty{isotope}'s type system, there remain many interesting questions with regards to, e.g., the representation of potentially nonterminating programs in a mathematically consistent manner (we currently use a nontermination monad, and are considering an approach similar to that in \cite{zombie}), and effective low-level representations for pattern matching on complex inductive types. One primary goal is to design an ergonomic API for dependently typed program construction in \tty{isotope} (since the language meant to be used programmatically rather than directly by a human user).

An especially exciting design problem to explore as an extension is lowering of \tty{isotope} programs to LLVM or Cranelift IR and from there to native programs, which, time permitting, we hope to attempt (the previous \tty{rain} prototype had some preliminary support for an LLVM-based JIT); further work could be done on optimizations leveraging dependent types, however, that is out of the scope of this project. One exciting research avenue a JIT backend would provide is to attempt to optimize type-checking itself by JITing functions appearing inside dependent types; a challenge with this approach is that it would require a well-defined ABI to some of the \tty{isotope} compiler's internals.


Another potential set of questions is raised by the problem of determining the best method to represent interactions with the operating system and heap in the context of dependent types with borrowing, especially in the context of nontermination; some preliminary work on this has been done. Other interesting research questions involve extending the dependent type theory underlying \tty{isotope} to include, e.g., an interval type, general higher inductive types, or homotopy types \cite{hottbook}. Another interesting direction would be exploring the interaction of \tty{isotope}-like type systems with directed notions of equality, as in \cite{dirhott}, in particular to model nondeterministic program execution and ``limited undefined behavior.''

\section{Draft Timetable}

The main goal of the project is to complete the design and implementation of the core \tty{isotope} language; after this, various extensions may be considered as detailed above.

\begin{itemize}

    \item End of May: have a draft implementation of the core \tty{isotope} language and a selection of example programs working. Have a draft write-up of \tty{isotope}'s implementation details, semantics, and type system.

    \item End of June: implement more advanced features, e.g. full inductive types, and explore and write up chosen extensions, time permitting, (e.g. LLVM, higher-order types, temporal edges).
    
    \item End of July: finish first draft of entire thesis incorporating implementation and extensions. Potentially do more work on polishing/extensions this month.

    \item End of August: revise and polish thesis, work on figures, etc.

\end{itemize}

\section{Previous Work}

There is a significant body of existing work on formally specifying and verifying the behavior of Rust programs. The RustBelt project (\cite{rustbelt}) has focused on using the concurrent separation logic Iris \cite{iris} to prove the soundness of the language's guarantees; namely, that if all \tty{unsafe} code in a program is Undefined Behaviour (UB) free, then the entire program is so as well. RustBelt provides methods to perform such proofs within Iris on a simplified subset of the Rust language, which the authors later used to verify significant portions of the standard library \cite{rustbelt}. The Stacked Borrows project \cite{stackedborrows} attempts to give a well-defined semantics to Rust's borrowing rules, allowing for further optimizations and new constructs to be placed on a well-defined footing. The Prusti \cite{prusti} project uses the VIPER infrastructure \cite{viper} to allow for the user-friendly verification of Rust programs, lifting information from the type system's ownership and borrowing discipline for use in separation-logic based proofs. 

There has been extensive work on using dependent types as an external logic to verify programs, e.g., the Verifiable C project \cite{verifiable-c}. In particular, Iris \cite{iris} itself uses the dependently typed language Coq \cite{coq}. The Low* \cite{low-star}, Idris \cite{idris-sys}, ATS \cite{ats}, and Deputy \cite{deputy} projects have explored the development of low-level dependently typed programming languages. Work on liquid typing \cites{liquid}{liquid-haskell} has explored the automatic verification of limited dependently-typed specifications using SAT solvers. There has also been extensive work on unifying linear and dependent types, such as \cite{lindep} and \cite{lindep-syn}, as well as applications, such as \cite{quantumlinear}. In unpublished work which they discussed with us, Nico Reismann and Helge Bahmann have explored the formalization of the RVSDG using Coq, as well as RVSDG-based program extraction.

\printbibliography

\end{document}